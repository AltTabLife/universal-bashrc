#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Define opening path
cd ~

# Bash Alisases
[ -f ~/.bash_aliases ] && . ~/.bash_aliases


alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '
PS1='\n\u >> \W:'

screenfetch

#Git setup
eval $(ssh-agent -s) &
ssh-add ~/.ssh/git &
