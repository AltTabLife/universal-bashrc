#!/bin/bash



#Standard Aliases
alias ll="ls -l"
alias lll='ls -l | less'
alias la="ls -la"
alias cdh="cd ~"
alias cdhl="cd ~; cll"
alias rmr="rm -r"


#Changing Directories
alias ..='cd ..'
alias ..l='cd ..;ll'
alias ...='cd ../..'
alias ....='cd ../../..'


#Windows Parallel
alias cls="clear"
alias cll="clear; ls -la"


#shortcuts
alias viba="vim ~/.bash_aliases"
alias viab="vim ~/.bash_aliases"
alias vibr="vim ~/.bashrc"
alias sbc="source ~/.bashrc"


alias cdpl="cd ~/playground; ll"
alias cdpol="cd ~/playground; ll"

alias vimqt='vim ~/.config/qtile/config.py'
alias cdqt='cd ~/playground/qtile/qtile/'

#Web management
#alias firefox='firefox &'

#Git management

alias alias_update='cp ~/.bash_aliases ~/git/universal-bashrc/bash_aliases'


#Arch Specific
alias pacman='sudo pacman'
alias vimqt='vim ~/.config/qtile/config.py'

alias emerald='visualboyadvance-m ~/games/roms/Pokemon\ -\ Emerald\ Version\ \(U\).gba'

#Functions

hid(){
	ls -a "$@" | grep '^\.';
}
